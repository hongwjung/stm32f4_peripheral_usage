/*
 * SystemTypes.h
 *
 *  Created on: Aug 21, 2014
 *      Author: ser
 */

#ifndef SYSTEMTYPES_H_
#define SYSTEMTYPES_H_

#include "stm32f4xx_conf.h"
#include "stm32f4xx.h"

#ifdef __cplusplus
	extern "C"{
#endif

typedef enum
{
	OK = 0,
	Error,
	WrongParameter,
	WrongMessage,
}Error_t;

typedef struct
{
	uint32_t gPin;
	GPIO_TypeDef* gPort;
}Gpio_t;


#ifdef __cplusplus
	}
#endif
#endif /* SYSTEMTYPES_H_ */
