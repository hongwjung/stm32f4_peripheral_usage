/*
 * CSerialHandler.h
 *
 *  Created on: Aug 22, 2014
 *      Author: ser
 */

#ifndef CSERIALHANDLER_H_
#define CSERIALHANDLER_H_

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"

class CSerialHandler {
	 xTaskHandle taskHandle;
	 xTaskHandle parentHandler;
public:
	void run();
	CSerialHandler(const char *name, unsigned short stackDepth, char priority,xTaskHandle& parentHandler_ );
	virtual ~CSerialHandler();
};

#endif /* CSERIALHANDLER_H_ */
