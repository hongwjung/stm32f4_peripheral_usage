/*
 * LCDHandlerTask.cpp
 *
 *  Created on: Aug 22, 2014
 *      Author: ser
 */

#include "LCDHandler.h"
#include "HW.h"
#include "QueueBox.h"
#include "BSPConfig.h"
#include "string.h"

extern "C" void pvLCDTaskCode(void *pvParameters) {
    (static_cast<LCDHandler*>(pvParameters))->run();
}

LCDHandler::LCDHandler(const char *name, unsigned short stackDepth, char priority)
{
	xTaskCreate(pvLCDTaskCode, (const char *) name, stackDepth, (void*) this, priority, &taskHandle);
}

LCDHandler::~LCDHandler()
{

}

void LCDHandler::run()
{
	char LcdBuf[CHAR_IN_LINE];
	char RecirvedLcdBuf[MessageMaxSize];
	char countStr = 0;
	while(1)
	{
		QueueBox::getIQueue().LCDQueue.receive(RecirvedLcdBuf);
		while(RecirvedLcdBuf[countStr++]);
		if (countStr < CHAR_IN_LINE)
		{
			memcpy(LcdBuf,RecirvedLcdBuf, countStr);
			HW::getHW().LCD4002.LCD_String(LcdBuf, sizeof(LcdBuf));
		}
		else
		{
			int countPerStr;
			for(countPerStr = countStr;countPerStr > CHAR_IN_LINE;countPerStr-=CHAR_IN_LINE)
			{
				memcpy(LcdBuf,&RecirvedLcdBuf[countStr - countPerStr], CHAR_IN_LINE-1);
				HW::getHW().LCD4002.LCD_String(LcdBuf, sizeof(LcdBuf));
			}
			memcpy(LcdBuf,&RecirvedLcdBuf[countStr - countPerStr], countStr);
			HW::getHW().LCD4002.LCD_Clear();
			HW::getHW().LCD4002.LCD_String(LcdBuf, sizeof(LcdBuf));
		}

		vTaskDelay(100);
	}
}

