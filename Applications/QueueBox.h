/*
 * QueueBox.h
 *
 *  Created on: Aug 24, 2014
 *      Author: ser
 */

#ifndef QUEUEBOX_H_
#define QUEUEBOX_H_

#include "QueueWrapper.h"
#include "SystemConfig.h"
#include "BSPConfig.h"

class QueueBox{
public:
	Queue LCDQueue;
	Queue SerialQ;
	static QueueBox& getIQueue(void);
private:
	QueueBox();
	virtual ~QueueBox();
};

#endif /* QUEUEBOX_H_ */
