/*
 * CMainHandler.cpp
 *
 *  Created on: Aug 22, 2014
 *      Author: ser
 */

#include <stdlib.h>
#include "HWTest.h"
#include "CMainHandler.h"
#include "SystemConfig.h"
#include "CSerialHandler.h"
#include "LCDHandler.h"
#include "HW.h"
#include "ff.h"
#include "QueueBox.h"

FATFS FatFs;

extern "C" void pvMAINaskCode(void *pvParameters) {
	(static_cast<CMainHandler*>(pvParameters))->run();
}

CMainHandler::CMainHandler(const char *name, unsigned short stackDepth, char priority) {
	// TODO Auto-generated constructor stub
	xTaskCreate(pvMAINaskCode, (const char *) name, stackDepth, (void*) this, priority, &taskHandle);
}

CMainHandler::~CMainHandler() {
	// TODO Auto-generated destructor stub
	/* Close the file */
	f_close(&inputFile);
}
void CMainHandler::TaskSuspend()
{
	 vTaskSuspend( this->taskHandle );
}
void CMainHandler::run()
{
	uint32_t	SendTimeDelay;
	/*Hardware Initialization*/
	HW::getHW();
	/*Tasks Initialization*/
	LCDHandler LCDHandlerTask("LCD Handler", LCD_HANDLER_STACK_SIZE , LCD_HANDLER_PRIORITY);
	CSerialHandler	SerialHandlerTask( "Serial Handler", SERIAL_HANDLER_STACK_SIZE, SERIAL_HANDLER_PRIORITY, this->taskHandle);
	QueueBox::getIQueue();
	/*FS Initialization*/
	char DelayBuffer[5]; 					/* Time delay */
	char OutStringBuffer[CHAR_IN_LINE];		/*Output string */
	FRESULT fr, frDelay, frString;    		/* FatFs return code */
	BYTE vol_ = 0;
	bool EndFlag = true;
	fr = f_mount(vol_, &FatFs);
	/* Open a text file */
	frDelay = f_open(&inputFile, "message.txt", FA_READ);
	frString = f_open(&inputQueue, "queue.txt", FA_READ);
	while(1)
	{
		if(fr == FR_OK & EndFlag)
		{
			/* Read all lines with time delay */
			if( f_gets(DelayBuffer, sizeof DelayBuffer, &inputFile) == NULL)
			{
				EndFlag = false;//End program
			}
			f_gets(OutStringBuffer, sizeof OutStringBuffer, &inputQueue);
			SendTimeDelay = (atoi(DelayBuffer))*1000;
			QueueBox::getIQueue().LCDQueue.send(OutStringBuffer);
		}
		vTaskDelay(SendTimeDelay);
	}
}
