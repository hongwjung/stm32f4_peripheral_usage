/*
 * ClassMessage.h
 *
 *  Created on: Aug 25, 2014
 *      Author: ser
 */

#ifndef CLASSMESSAGE_H_
#define CLASSMESSAGE_H_

#include "SystemConfig.h"
#include "SystemTypes.h"

class ClassMessage {

public:
	enum PackageType
	{
		System = 1,
		LCDData
	};

	enum Position
	{
		STX_POSITION,
		LEN_POSITION,
		TYPE_POSITION,
		DATA_POSITION,
		CRC_POSITION = DATA_POSITION,
		CRC_POSITION_2,
		ETX_POSITION,
	};

	ClassMessage();
	Error_t getMessage(unsigned char * InputMessage,  short* lengh);
	virtual ~ClassMessage();

private:

    char STX = MESSAGE_START;
    char Lenth;
    char PType;
    char* Data;
    unsigned short CRC16;
    char ETX = MESSAGE_END;

    const char overHead = sizeof(STX) + sizeof(Lenth) + sizeof(PType) + sizeof(CRC16) + sizeof(ETX);
    const char prefix = sizeof(STX) + sizeof(Lenth) + sizeof(PType);
    const short maxPackageLength = 1017;
public:

};

#endif /* CLASSMESSAGE_H_ */
