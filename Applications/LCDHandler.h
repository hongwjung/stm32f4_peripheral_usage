/*
 * LCDHandlerTask.h
 *
 *  Created on: Aug 22, 2014
 *      Author: ser
 */

#ifndef LCDHANDLERTASK_H_
#define LCDHANDLERTASK_H_

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"

class LCDHandler{
	 xTaskHandle taskHandle;
public:
	void run();
	LCDHandler(const char *name, unsigned short stackDepth, char priority);
	~LCDHandler();
};

#endif /* LCDHANDLERTASK_H_ */
