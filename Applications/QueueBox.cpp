/*
 * QueueBox.cpp
 *
 *  Created on: Aug 24, 2014
 *      Author: ser
 */

#include "QueueBox.h"
#include "SystemConfig.h"
#include "BSPConfig.h"

QueueBox::QueueBox() {
	// TODO Auto-generated constructor stub
	 LCDQueue.Create( AmountOfMessage, MessageMaxSize );
	 SerialQ.Create( AmountOfSerialMessage, MessageMaxSize );
}

QueueBox::~QueueBox() {
	// TODO Auto-generated destructor stub
}

QueueBox& QueueBox::getIQueue()
{
	static QueueBox InstaceQ;
	return InstaceQ;
}

