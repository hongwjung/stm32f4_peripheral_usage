/*
 * SystemConfig.h
 *
 *  Created on: Aug 22, 2014
 *      Author: ser
 */

#ifndef SYSTEMCONFIG_H_
#define SYSTEMCONFIG_H_

#define MESSAGE_START			'2'
#define MESSAGE_END				'3'

#define AmountOfMessage			5
#define AmountOfSerialMessage	2
#define MessageMaxSize			254
/* Priorities for the application tasks. */
#define LCD_HANDLER_PRIORITY					( tskIDLE_PRIORITY + 3UL )
#define SERIAL_HANDLER_PRIORITY					( tskIDLE_PRIORITY + 2UL )
#define MAIN_HANDLER_PRIORITY					( tskIDLE_PRIORITY + 1UL )

#define LCD_HANDLER_STACK_SIZE			1024
#define SERIAL_HANDLER_STACK_SIZE		512
#define MAIN_HANDLER_STACK_SIZE			2048

#endif /* SYSTEMCONFIG_H_ */
