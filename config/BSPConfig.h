/*
 * BSPConfig.h
 *
 *  Created on: Aug 21, 2014
 *      Author: ser
 */

#ifndef BSPCONFIG_H_
#define BSPCONFIG_H_
#ifdef __cplusplus
	extern "C"{
#endif

#define	RS232_Dev		USART2
#define RS232_PIN_TX	GPIO_Pin_2
#define	RS232_PIN_RX	GPIO_Pin_3
#define RS232_PORT		GPIOD

#define BUTTON_PIN
#define	BUTTON_PORT

#define LCD_GPIO 		GPIOA
#define LCD_D4 			GPIO_Pin_5
#define LCD_D5 			GPIO_Pin_6
#define LCD_D6 			GPIO_Pin_8
#define LCD_D7 			GPIO_Pin_7
#define LCD_RS 			GPIO_Pin_0
#define LCD_RW 			GPIO_Pin_1
#define LCD_EN 			GPIO_Pin_4
#define CHAR_IN_LINE    40

#define USB_PORT		GPIOA
#define VBUS_PIN		GPIO_Pin_9
#define DM_PIN			GPIO_Pin_11
#define DP_PIN			GPIO_Pin_12



#ifdef __cplusplus
	}
#endif
#endif /* BSPCONFIG_H_ */
