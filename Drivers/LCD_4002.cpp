/*
 * LCD_44780.c
 *
 *  Created on: Aug 9, 2012
 *      Author: arm
 */
#include "LCD_4002.h"
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "BSPConfig.h"

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"

//Delay us
void delay_us(unsigned int value)
{
	value = value*200;
	for(; value!=0 ;value--);
}

GPIO_InitTypeDef GPIO_InitStructure;
void delayms(unsigned int value)
{
	value = value*200000;
	for(; value!=0 ;value--);

}
/**
  * @brief  Write half bute data to the LCD
  * @param  nibbleToWrite nibble byte
  * @retval None
*/
void LCD_WriteNibble(u8 nibbleToWrite)
{
    GPIO_WriteBit(LCD_GPIO, LCD_EN, Bit_SET);
    GPIO_WriteBit(GPIOA, LCD_D4, (BitAction)(nibbleToWrite & 0x01));
    GPIO_WriteBit(GPIOA, LCD_D5, (BitAction)(nibbleToWrite & 0x02));
    GPIO_WriteBit(GPIOA, LCD_D6, (BitAction)(nibbleToWrite & 0x04));
    GPIO_WriteBit(GPIOA, LCD_D7, (BitAction)(nibbleToWrite & 0x08));
    GPIO_WriteBit(LCD_GPIO, LCD_EN, Bit_RESET);
}
/**
  * @brief Read half byte command
  * @param  None
  * @retval Read data
*/
u8 LCD_ReadNibble(void)
{
    u8 tmp = 0;
    GPIO_WriteBit(LCD_GPIO, LCD_EN, Bit_SET);
    tmp |= (GPIO_ReadInputDataBit(GPIOA, LCD_D4) << 0);
    tmp |= (GPIO_ReadInputDataBit(GPIOA, LCD_D5) << 1);
    tmp |= (GPIO_ReadInputDataBit(GPIOA, LCD_D6) << 2);
    tmp |= (GPIO_ReadInputDataBit(GPIOA, LCD_D7) << 3);
    GPIO_WriteBit(LCD_GPIO, LCD_EN, Bit_RESET);
    return tmp;
}
/**
  * @brief  Read data from LCD
  * @param  None
  * @retval Read data
*/
u8 LCD_ReadStatus(void)
{
    u8 status = 0;

    GPIO_InitStructure.GPIO_Pin   =  LCD_D4 | LCD_D5 | LCD_D6 | LCD_D7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_WriteBit(LCD_GPIO, LCD_RW, Bit_SET);
    GPIO_WriteBit(LCD_GPIO, LCD_RS, Bit_RESET);

    status |= (LCD_ReadNibble() << 4);
    status |= LCD_ReadNibble();

    GPIO_InitStructure.GPIO_Pin   =  LCD_D4 | LCD_D5 | LCD_D6 | LCD_D7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    return status;
}
/**
  * @brief  Write to the DDRAM
  * @param  commandToWrite - code of command
  * @retval None
*/
void LCD_WriteData(u8 dataToWrite)
{
    GPIO_WriteBit(LCD_GPIO, LCD_RW, Bit_RESET);
    GPIO_WriteBit(LCD_GPIO, LCD_RS, Bit_SET);
    LCD_WriteNibble(dataToWrite >> 4);
    delayms(1);
    LCD_WriteNibble(dataToWrite & 0x0F);
    delayms(1);

}
/**
  * @brief  Write byte command
  * @param  commandToWrite - code of command
  * @retval None
*/
void LCD_WriteCommand(u8 commandToWrite)
{
    GPIO_WriteBit(LCD_GPIO, LCD_RS, Bit_RESET);
    GPIO_WriteBit(LCD_GPIO, LCD_RW , Bit_RESET);
    LCD_WriteNibble(commandToWrite >> 4);
    delayms(5);
    LCD_WriteNibble(commandToWrite & 0x0F);
    delayms(5);
}
/**
  * @brief  Write half byte command
  * @param  commandToWrite - code of command
  * @retval None
*/
void LCD_WriteShortCommand(u8 commandToWrite)
{
    GPIO_WriteBit(LCD_GPIO, LCD_RW | LCD_RS, Bit_RESET);
    LCD_WriteNibble(commandToWrite >> 4); //Shift Right
    delayms(5);
}
/**
  * @brief  Set cursor to position
  * @param  str - position string
  * @param	col - column of the string
  * @retval None
*/
void CLCD::LCD_Goto (char str, char col)
{
    LCD_WriteCommand((128|((0x40*str)+col)));
}
/**
  * @brief  Set cursor to position
  * @param  ADR - position address
  * @retval None
*/
void CLCD::LCD_Goto (uint32_t ADR)
{
    if (ADR>=CharInLine) ADR=ADR+64-(CharInLine);
    LCD_WriteCommand (ADR|(1<<7));
}
/**
  * @brief  Put char on LCD
  * @param  DAT - char data
  * @retval None
*/
void CLCD::LCD_Putchar (unsigned int DAT)
{
	LCD_WriteData(DAT);
}
/**
  * @brief  Put string on LCD
  * @param  STRING - pointer to string
  * @retval None
*/
void CLCD::LCD_String (const char *STRING)
{
    while (*STRING)
    {
    	LCD_WriteData (*STRING);
    	STRING++;
    }
}
/**
  * @brief  Put string on LCD
  * @param  STRING - pointer to string
  * @param	buf_size - input buffer size
  * @retval None
*/
void CLCD::LCD_String (const char *STRING, char buf_size)
{
	buf_size += buf_size;
	char symboCount = 0;
    while (buf_size--)
    {
    	if(*STRING && *STRING !='\n' && CharInLine>=symboCount)
    	{
    		LCD_WriteData (*STRING);
    		STRING++;
    		symboCount++;
    	}
    	else
    	{
    		LCD_WriteData(' ');
    	}
    }
}

void LCD_View_Mode (u32 cursor1, u32 cursor2, u32 view_on)
{
    volatile u32 tmp=(1<<3);
    if (cursor1) tmp|=(1<<1);
    if (cursor2) tmp|=(1<<0);
    if (view_on) tmp|=(1<<2);
    LCD_WriteShortCommand(tmp);
}

void CLCD::LCD_Clear(void)
{
	LCD_WriteCommand(0x01);
	delayms(10);
}

CLCD::CLCD()
{
	CharInLine = 0;
}

CLCD::~CLCD()
{
}


/**
  * @brief  LCD Initialization
  * @param  CharInLine_ - amount chars in line
  * @retval None
*/
void CLCD::Init(uint8_t CharInLine_)
{
    GPIO_InitTypeDef io_init;
    CharInLine = CharInLine_;

    /* settings for both pa and pc */
    io_init.GPIO_Mode = GPIO_Mode_OUT;
    io_init.GPIO_OType = GPIO_OType_PP;
    io_init.GPIO_Speed = GPIO_Speed_100MHz;
    io_init.GPIO_PuPd = GPIO_PuPd_NOPULL;

    /* init pa */
    io_init.GPIO_Pin = LCD_D4 | LCD_D5 | LCD_D6 | LCD_D7 | LCD_RS | LCD_RW | LCD_EN;
    GPIO_Init(GPIOA, &io_init);

    vTaskDelay(15);
    LCD_WriteShortCommand(0x30);
    delayms(5);
    LCD_WriteShortCommand(0x30);
    delay_us(100);
    LCD_WriteShortCommand(0x30);
    delay_us(100);
    LCD_WriteShortCommand(0x20);
	LCD_WriteCommand (0x28);	// function set: 4 bits, 1 line, 5x8 dots
	delay_us(50);
	LCD_WriteCommand(0x0C);  	// display control: turn display on, cursor off, no blinking
	LCD_WriteCommand(0x06);  	// entry mode set: increment automatically, display shift, right shift
	vTaskDelay(40);
	LCD_Clear();


}

