/*
 * HWInitialization.cpp
 *
 *  Created on: Aug 21, 2014
 *      Author: ser
 */
/* Includes ------------------------------------------------------------------*/
#include "HW.h"
#include "stm32f4xx_conf.h"
#include "stm32f4_discovery.h"
#include "BSPConfig.h"

#include "usbd_cdc_core.h"
#include "usbd_usr.h"
#include "usbd_desc.h"
#include "usbd_cdc_vcp.h"
#include "usb_dcd_int.h"

__ALIGN_BEGIN extern USB_OTG_CORE_HANDLE  USB_OTG_dev __ALIGN_END;

/**
  * @brief  Board's peripherals initialization
  * @param  None
  * @retval None
  */
HW::HW()
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	/* Initialize LEDS */
	STM_EVAL_LEDInit(LED3);
	STM_EVAL_LEDInit(LED5);
	STM_EVAL_LEDInit(LED6);
	/*LCD Init*/
	LCD4002.Init(CHAR_IN_LINE);
	/* Initialize User Button */
	STM_EVAL_PBInit(BUTTON_USER, BUTTON_MODE_EXTI);
	/*Usb initialization*/
	USBD_Init(&USB_OTG_dev, USB_OTG_FS_CORE_ID, &USR_desc, &USBD_CDC_cb, &USR_cb);
	/*RS232 Init*/
	SerialInterface.Init(RS232_PIN_TX, RS232_PIN_RX, RS232_PORT, RS232_Dev);

}

HW::~HW() {
	// TODO Auto-generated destructor stub
}
/**
  * @brief  Get hardware board instance
  * @param  None
  * @retval None
  */
HW& HW::getHW()
{
	static HW SinleObgect;
	return SinleObgect;
}
/**
  * @brief  Initialization RCC for active device
  * @param  None
  * @retval None
  */
void RCC_Initialization(void)
{
	//Enable the GPIO pins
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
}
