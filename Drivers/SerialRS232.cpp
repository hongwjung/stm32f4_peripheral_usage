#include "SerialRS232.h"
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

static void NVIC_Config(void);

SerialRS232::SerialRS232()
{
	USARTx = NULL;
}
/**
  * @brief  Initialize UART device
  * @param  GPIOPinTx - Pinout mask
  * @param  GPIOPinRx - Pinout mask
  * @param  gPort - Pinout port
  * @param	_USARTx - Name of device
  * @retval None
  */
void SerialRS232::Init(uint32_t GPIOPinTx, uint32_t GPIOPinRx, GPIO_TypeDef* gPort, USART_TypeDef* _USARTx )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	USARTx = _USARTx;
	EnableUARTDevice();
	//Connect GPIO pins to the UART
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2 ,GPIO_AF_USART2); //TX
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3 ,GPIO_AF_USART2); //RX
	//Configure the GPIO Pins
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

	GPIO_InitStructure.GPIO_Pin = GPIOPinTx | GPIOPinRx;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_Init(gPort, &GPIO_InitStructure);

	//Initialize the USART
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USARTx, &USART_InitStructure);
	NVIC_Config();
	//Enable (UE) the USART
	USART_Cmd(USARTx, ENABLE);
	/* Enable the interrupt */
	USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);
}

SerialRS232::~SerialRS232()
{
}
/**
  * @brief  Send buf to UART
  * @param  buf - Pointer to the buffer
  * @param  cnt - buffer size
  * @retval None
  */
void SerialRS232::Send(uint8_t *buf, uint32_t cnt)
{
	while(cnt>1)
	{
		//Write data to DR
		USARTx->DR = (*buf & (uint16_t)0x01FF);
		buf++;
		//Wait for DR ready
		while (!(USARTx->SR & USART_SR_TXE))
		{};
		cnt--;
	}
	//Send the last byte
	USARTx->DR = (*buf & (uint16_t)0x01FF);
	//Wait for the end of actual transmission
	while (!(USARTx->SR & USART_SR_TC))
	{};   //Not this will be cleared only after a read from SR and then write to DR. (Therefore after this operation, it will still be set)
}
/**
  * @brief  Send C style string to UART
  * @param  buf - Pointer to the string buffer
  * @retval None
  */
void SerialRS232::SendLine(char *buf)
{
	uint32_t i=256; //Max line length
	while((i>0) & (*buf!='\n'))
	{
		//Write data to DR
		USARTx->DR = (*buf & (uint16_t)0x01FF);
		buf++;
		//Wait for DR ready
		while (!(USARTx->SR & USART_SR_TXE))
		{};
		i--;
	}
	//Send the last byte
	USARTx->DR = (*buf & (uint16_t)0x01FF);
	//Wait for the end of actual transmission
	while (!(USARTx->SR & USART_SR_TC))
	{};   //Not this will be cleared only after a read from SR and then write to DR. (Therefore after this operation, it will still be set)
}

/**
  * @brief  Receive data without interrupt
  * @param  None
  * @retval None
  */
void SerialRS232::Receive(uint8_t *buf, uint32_t cnt)
{
	while(cnt>0)
	{
		//Wait for new data
		while (!(USARTx->SR & USART_SR_RXNE))
		{};
		//Read the data
		*buf++=USARTx->DR;
		cnt--;
	}
}
/**
  * @brief  Select UART device
  * @param  None
  * @retval None
  */
Error_t  SerialRS232::EnableUARTDevice(void)
{
	switch((long unsigned int)this->USARTx)
	{
	case (long unsigned int)USART2:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
		break;
	case (long unsigned int)USART3:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
		break;
	case (long unsigned int)UART4:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);
		break;
	case (long unsigned int)UART5:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);
		break;
	case (long unsigned int)UART7:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART7, ENABLE);
		break;
	case (long unsigned int)UART8:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART8, ENABLE);
		break;
		default:
			return WrongParameter;
			break;
	}
	return OK;
}

/**
  * @brief  Configures the nested vectored interrupt controller.
  * @param  None
  * @retval None
  */
static void NVIC_Config(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Enable the USARTx Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}
