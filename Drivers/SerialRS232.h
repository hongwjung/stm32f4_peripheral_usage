#ifndef __UARTLIB_H
#define __UARTLIB_H

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

class SerialRS232
{
	USART_TypeDef* USARTx;
	Error_t	EnableUARTDevice(void);
public:
	SerialRS232();
	~SerialRS232();
	void Init(uint32_t GPIOPinTx, uint32_t GPIOPinRx, GPIO_TypeDef* gPort, USART_TypeDef* _USARTx);
	void Send(uint8_t *buf, uint32_t cnt);
	void Receive(uint8_t *buf, uint32_t cnt);
	void SendLine(char *buf);
};
#endif //__UARTLIB_H
