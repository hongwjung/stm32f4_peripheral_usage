/*
 * usb_cdc_test.cpp
 *
 *  Created on: Aug 20, 2014
 *      Author: ser
 */
/* Includes ------------------------------------------------------------------*/
#include "usbd_cdc_core.h"
#include "usbd_usr.h"
#include "usbd_desc.h"
#include "usbd_cdc_vcp.h"
#include "usb_dcd_int.h"
#include "HWTest.h"
/* Private define ------------------------------------------------------------*/
#define TESTRESULT_ADDRESS         0x080FFFFC
#define ALLTEST_PASS               0x00000000
#define ALLTEST_FAIL               0x55555555
/* Private variables ---------------------------------------------------------*/
/*
 * The USB data must be 4 byte aligned if DMA is enabled. This macro handles
 * the alignment, if necessary (it's actually magic, but don't tell anyone).
 */

#define BUF_IN_SIZE             512
uint8_t Buf_In[BUF_IN_SIZE];
uint32_t indxBuf_In_Write, indxBuf_In_Read;
uint32_t Buf_In_Len;

#define BUF_OUT_SIZE            512
uint8_t Buf_Out[BUF_OUT_SIZE];
uint32_t indxBuf_Out_Write, indxBuf_Out_Read;
uint32_t Buf_Out_Len;

uint32_t indxDiff (uint32_t indxWrite, uint32_t indxRead, uint32_t Buf_Size)
{
    if (indxWrite >= indxRead)
        return (indxWrite - indxRead);
    else
        return ((Buf_Size - indxRead) + indxWrite);
}

void HWTest::usb_cdc_test (void)
{
//	/* Setup USB */
//		USBD_Init(&USB_OTG_dev,
//		            USB_OTG_FS_CORE_ID,
//		            &USR_desc,
//		            &USBD_CDC_cb,
//		            &USR_cb);

	while(1)
	{
		/* If there's data on the virtual serial port:
		 *  - Echo it back
		 *  - Turn the green LED on for 10ms
		 */
		uint8_t theByte;
		if (VCP_get_char(&theByte))
		{
			VCP_put_char(theByte);
		}
	}
}

