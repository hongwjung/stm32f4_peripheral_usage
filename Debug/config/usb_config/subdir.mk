################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../config/usb_config/usb_bsp.c \
../config/usb_config/usbd_desc.c \
../config/usb_config/usbd_usr.c 

OBJS += \
./config/usb_config/usb_bsp.o \
./config/usb_config/usbd_desc.o \
./config/usb_config/usbd_usr.o 

C_DEPS += \
./config/usb_config/usb_bsp.d \
./config/usb_config/usbd_desc.d \
./config/usb_config/usbd_usr.d 


# Each subdirectory must supply rules for building sources it contributes
config/usb_config/%.o: ../config/usb_config/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mthumb-interwork -mlittle-endian -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall -Wextra  -g -ggdb -DDEBUG -DSTM32F40_41xxx -DUSE_HAL_DRIVER -DHSE_VALUE=8000000 -D"assert_param(expr)= ((void)0)" -I"/home/data/Projects/DrumModule/WS/stm32f4/inc" -I"/home/data/Projects/DrumModule/WS/stm32f4/system/CMSIS/core" -I"/home/data/Projects/DrumModule/WS/stm32f4/system/CMSIS/stm32" -I"/home/data/Projects/DrumModule/WS/stm32f4/system/STM32F4xx_StdPeriph_Driver/inc" -I"/home/data/Projects/DrumModule/WS/stm32f4/Drivers" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/fat_fs/inc" -I"/home/data/Projects/DrumModule/WS/stm32f4/config" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/FreeRTOS/include" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/FreeRTOS/portable" -I"/home/data/Projects/DrumModule/WS/stm32f4/unit_test" -I"/home/data/Projects/DrumModule/WS/stm32f4/config/usb_config" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/USB_Lib/cdc" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/USB_Lib/core" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/USB_Lib/otg" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/fat_fs" -I"/home/data/Projects/DrumModule/WS/stm32f4/Applications" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


