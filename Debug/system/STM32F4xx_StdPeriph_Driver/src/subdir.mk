################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../system/STM32F4xx_StdPeriph_Driver/src/misc.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_adc.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_can.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_crc.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_aes.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_des.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_tdes.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dac.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dbgmcu.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dcmi.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma2d.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_exti.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fsmc.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_gpio.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_md5.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_sha1.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_i2c.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_iwdg.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_ltdc.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_pwr.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rcc.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rng.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rtc.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sai.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sdio.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spi.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_syscfg.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_tim.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_usart.c \
../system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_wwdg.c 

OBJS += \
./system/STM32F4xx_StdPeriph_Driver/src/misc.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_adc.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_can.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_crc.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_aes.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_des.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_tdes.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dac.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dbgmcu.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dcmi.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma2d.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_exti.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fsmc.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_gpio.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_md5.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_sha1.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_i2c.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_iwdg.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_ltdc.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_pwr.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rcc.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rng.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rtc.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sai.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sdio.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spi.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_syscfg.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_tim.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_usart.o \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_wwdg.o 

C_DEPS += \
./system/STM32F4xx_StdPeriph_Driver/src/misc.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_adc.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_can.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_crc.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_aes.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_des.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_tdes.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dac.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dbgmcu.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dcmi.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma2d.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_exti.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fsmc.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_gpio.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_md5.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_sha1.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_i2c.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_iwdg.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_ltdc.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_pwr.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rcc.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rng.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rtc.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sai.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sdio.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spi.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_syscfg.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_tim.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_usart.d \
./system/STM32F4xx_StdPeriph_Driver/src/stm32f4xx_wwdg.d 


# Each subdirectory must supply rules for building sources it contributes
system/STM32F4xx_StdPeriph_Driver/src/%.o: ../system/STM32F4xx_StdPeriph_Driver/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mthumb-interwork -mlittle-endian -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall -Wextra  -g -ggdb -DDEBUG -DSTM32F40_41xxx -DUSE_HAL_DRIVER -DHSE_VALUE=8000000 -D"assert_param(expr)= ((void)0)" -I"/home/data/Projects/DrumModule/WS/stm32f4/inc" -I"/home/data/Projects/DrumModule/WS/stm32f4/system/CMSIS/core" -I"/home/data/Projects/DrumModule/WS/stm32f4/system/CMSIS/stm32" -I"/home/data/Projects/DrumModule/WS/stm32f4/system/STM32F4xx_StdPeriph_Driver/inc" -I"/home/data/Projects/DrumModule/WS/stm32f4/Drivers" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/fat_fs/inc" -I"/home/data/Projects/DrumModule/WS/stm32f4/config" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/FreeRTOS/include" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/FreeRTOS/portable" -I"/home/data/Projects/DrumModule/WS/stm32f4/unit_test" -I"/home/data/Projects/DrumModule/WS/stm32f4/config/usb_config" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/USB_Lib/cdc" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/USB_Lib/core" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/USB_Lib/otg" -I"/home/data/Projects/DrumModule/WS/stm32f4/Third_Party/fat_fs" -I"/home/data/Projects/DrumModule/WS/stm32f4/Applications" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


